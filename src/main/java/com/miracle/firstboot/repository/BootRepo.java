package com.miracle.firstboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.miracle.firstboot.model.Employee;

@Repository
public interface BootRepo extends JpaRepository<Employee, Integer>{
	
	

}
