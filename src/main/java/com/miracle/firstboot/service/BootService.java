package com.miracle.firstboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miracle.firstboot.model.Employee;
import com.miracle.firstboot.repository.BootRepo;

@Service
public class BootService {
	
	@Autowired
	BootRepo bootRepo;
	
	
	public List<Employee> getEmployees(){
		
		return bootRepo.findAll();
	}

}
