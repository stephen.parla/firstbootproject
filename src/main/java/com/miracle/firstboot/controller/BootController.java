package com.miracle.firstboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miracle.firstboot.model.Employee;
import com.miracle.firstboot.service.BootService;

@RestController
public class BootController {
	
	@Autowired
	BootService bootService;
	
	@GetMapping(value = "/allEmployees")
	public List<Employee> getAllEmpllyees(){
		
		return bootService.getEmployees();
	}

}
